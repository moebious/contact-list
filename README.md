

## Contact List with Casandra


This contact application includes

1. Create
2. Read
3. Update
4. Delete


I was unable to get the solr search working. I did create the schema.xml and solrconfig.xml and they seem to load via the add_schema.sh described in this readme but i never could get a query to run in the cqlsh prompt. I was also never able to connect to the cassandra dse running on the vm, from my host machine (macosx). I am sorry i was unable to complete the task as given. However I do want to say thank you in advance for taking a look at what I have to present here and for giving me the opportunity.


Some things that i added

Server-Side

Wrote some code that generates the cql based on the model and i also included some unit tests for that.

Wrapped my calls to the cassandra-driver in Promises.

Client Side

Included and used a validation library to validate user input.

Used the react-bootstrap library and components to build the forms an other elements in the views.

Went very minimalistic on state. There is a ContactStore object that makes raw XMLHttpRequest calls to the server. In the interest of time I did not go with redux or some other library.


### Search without solr

I did go ahead and implement the search using a separate data store in the branch called searchNonSolr


##Instructions

1. git clone the repository
```git clone git@bitbucket.org:moebious/contact-list.git```

2. copy the api/scripts folder to the demos folder on your VM (this is assuming you have the cassandra VM appliance)

```cp -R contact-list/api/scripts dse/nodes/node1/demos/scripts```

3. cd to the scripts folder and make the add_schema.sh executable

```cd dse/nodes/node1/demos/scripts```
```chmod +x add_schema.sh```

4. run it

```./add_schema.sh```

this should create the contactdemo schema and the contacts table

5. from the root of the application directory (where ever you cloned the repo)
cd to the api folder and run
```npm install```
```npm start```

6. from the root of the application directory, cd to the frontend directory and run

```npm install```

```npm start```

7. point your browser to http://localhost:8080
