var chai = require('chai');
var expect = chai.expect;

var batchCreator = require('../service/batchHelper');
var tag = require('../model/tag');
var helper = require('../service/queryHelper');
var idService = require('../service/idService');

var tagCqlHelper = helper(tag);

describe("batches", function() {
  describe("batches", function() {
    it("return an array with the correct length", function() {
      var batch = batchCreator(tagCqlHelper, idService);
      var queries = batch.batches({
          "contactid": idService.Id(),
          "first_name": "Barry",
          "last_name": "Manalow",
          "email": "barry@john.com",
          "comments": "wow i havnt heard about this guy since the moon shot",
          "phone": "389-5540"
        });
      expect(queries.length === 17).to.be.equal(true)

    })
  })
})
