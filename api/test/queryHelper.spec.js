var chai = require('chai');
var expect = chai.expect;
var queryHelper = require('../service/queryHelper');
var idService = require('../service/idService')


describe("ddlHelper", function() {
  beforeEach(function(){
    this.model = {
      name: "contact",
      columns: {
        contactid: {
          type: "string",
          primarykey: true
        },
        first_name: {
          type: "string"
        },
        last_name: {
          type: "string"
        },
        email: {
          type: "string"
        },
        comments: {
          type: "string"
        },
        phone:{
          type: "string"
        }
      }
    }
  })

  describe("select", function() {
    it("create a proper select statement", function() {
      var helper = queryHelper(this.model);
      var statement = helper.select();
      //console.log(statement)
      expect(statement.indexOf('select')>-1).to.equal(true)
      var keys = Object.keys(this.model.columns);
      keys.forEach(function(key) {
        expect(statement.indexOf(key)>-1).to.equal(true)
      });

      expect(statement.indexOf(this.model.name)>-1).to.equal(true)
      expect(statement.indexOf('from')>-1).to.equal(true)
      expect(statement.split(",").length === keys.length).to.be.equal(true)
    })
  });
  describe("selectOne", function() {
    it("create a select statement by primarykey", function() {
      var helper = queryHelper(this.model);
      var statement = helper.selectOne();
      //console.log(statement)
      expect(statement.indexOf('select')>-1).to.equal(true)
      var keys = Object.keys(this.model.columns);
      keys.forEach(function(key) {
        expect(statement.indexOf(key)>-1).to.equal(true)
      });

      expect(statement.indexOf(this.model.name)>-1).to.equal(true)
      expect(statement.indexOf('from')>-1).to.equal(true)
      expect(statement.split(",").length === keys.length).to.be.equal(true)
    })
  });
  describe("insert", function() {
    it("creates a proper insert statement", function() {
      var helper = queryHelper(this.model);
      var insertedObject = {
          "contactid": (+new Date() + Math.floor(Math.random() * 999999)).toString(36),
          "first_name": "Barry",
          "email": "barry@john.com",
          "comments": "wow i havnt heard about this guy since the moon shot",
          "phone": "389-5540"
        }
      var query = helper.insert(insertedObject);
      //console.log(query.statement)
      //console.log(query.params)
      expect(query.statement.indexOf('insert') > -1, 'insert is not there').to.be.equal(true);
      var keys = Object.keys(insertedObject);
      keys.forEach(function(key) {
        //expect(query.statement.indexOf(key)>-1).to.equal(true)
      });

      expect(query.statement.indexOf('values')>-1, 'comments are not there').to.equal(true)
      expect(query.statement.indexOf(this.model.name)>-1, 'model name is not there').to.equal(true)
      expect(query.statement.split(",").length === ((keys.length * 2) -1), "wrong number of commas" ).to.be.equal(true)


    });

    it("throws an error when you put an unknown key on the object to be inserted", function() {
      var helper = queryHelper(this.model);
      var insertedObject = {
          "contactid": (+new Date() + Math.floor(Math.random() * 999999)).toString(36),
          "first_name": "Barry",
          "fname": "erronious",
          "email": "barry@john.com",
          "comments": "wow i havnt heard about this guy since the moon shot",
          "phone": "389-5540"
        }

      expect(function(){
        var query = helper.insert(insertedObject);
      }).to.throw()



    });

  });
  describe("update", function(){
    it("creates a proper prepared update statement for the given model", function() {
      var helper = queryHelper(this.model);
      var query = helper.update({
          "contactid": (+new Date() + Math.floor(Math.random() * 999999)).toString(36),
          "first_name": "Barry",
          "last_name": "Manalow",
          "email": "barry@john.com",
          "comments": "wow i havnt heard about this guy since the moon shot",
          "phone": "389-5540"
        });
      //console.log(query.statement);
      expect(query.statement.indexOf('update') > -1, 'insert is not there').to.be.equal(true);
      expect(query.statement.indexOf('set') > -1, 'set is not there').to.be.equal(true);
      expect(query.statement.indexOf(this.model.name)>-1, 'model name is not there').to.equal(true)
      var keys = Object.keys(this.model.columns);
      keys.forEach(function(key) {
        expect(query.statement.indexOf(key)>-1, key + " is not there").to.equal(true)
      });

    })

    it("throws an error when you put an unknown key on the object to be updated", function() {
      var helper = queryHelper(this.model);
      var updateObject = {
          "contactid": (+new Date() + Math.floor(Math.random() * 999999)).toString(36),
          "first_name": "Barry",
          "last_name": "Manalow",
          "fname": "erronious",
          "email": "barry@john.com",
          "comments": "wow i havnt heard about this guy since the moon shot",
          "phone": "389-5540"
        }

        expect(function(){
          var query = helper.update(updateObject);
        }).to.throw()

    })


  })
  describe("delete", function(){
    it("creates a proper prepared delete statement for the given model", function() {
      var helper = queryHelper(this.model);
      var query = helper.delete({
          "contactid": idService.Id(),
          "first_name": "Barry",
          "last_name": "Manalow",
          "email": "barry@john.com",
          "comments": "wow i havnt heard about this guy since the moon shot",
          "phone": "389-5540"
        });
      //console.log(query.statement);
      expect(query.statement.indexOf('delete from') > -1, 'delete is not there').to.be.equal(true);
      expect(query.statement.indexOf(this.model.name)>-1, 'model name is not there').to.equal(true)
    })
  })

})
