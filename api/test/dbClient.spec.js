var chai = require('chai');
var expect = chai.expect;

var db = require('../service/dbClient');
var client = {
  execute: function(cql, params, options, cb){
    cb(null, [{id: 3, name: "david"}])
  }
}
var errorClient = {
  execute: function(cql, params, options, cb){
    cb({message: "an error occurred"}, [{id: 3, name: "david"}])
  }
}

describe("dbClient", function() {


  describe("#exec()", function() {
    it("runs the client execute function", function(done){
      var database = db(client);
      database.exec("hello", null, null)
        .then(function(result) {
          expect(result[0].name).to.be.equal("david");
          done()
        })

    });
    it("returns an err message", function(done) {
      var database = db(errorClient);
      database.exec("hello", null, null)
        .then(function(result) {
          expect(result[0].name).to.be.equal("david");
          done()
        })
        .catch(function(err) {
          expect(err.message).to.be.equal("an error occurred");
          done();
        })

    })
  })
})
