var Promise = require('bluebird');

function db(client) {

  function exec(cql, params, options){
    return new Promise(function(resolve, reject){
      try{
        client.execute(cql, params, options, function(err, result) {
          if(err){
            return reject(err)
          }
          resolve(result)
        });

      }catch(e){
        return reject(e)
      }

    });
  }

  function batch(queries){
    return new Promise(function(resolve, reject) {
      console.log('queries ', queries)
      client.batch(queries, {prepare: true}, function(err){
        if(err){
          return reject(err);
        }
        resolve({
          message: "done"
        });
      });
    });
  }

  return {
    exec: exec,
    batch: batch
  }
}

module.exports = db;
