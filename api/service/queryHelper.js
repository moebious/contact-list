
function ddl(model){
  var thisModel = model;

  function createSelect(){
    var ddl = "select ";
    var keys = Object.keys(thisModel.columns);
    keys.forEach(function(key, i) {
      ddl +=  key + (i === keys.length-1? " " : ", ");
    })
    ddl += " from " + thisModel.name
    return ddl;
  }

  function createSelectOne(){
    var ddl = createSelect();
    ddl += " where " + getPrimary() + " =?"
    return ddl;
  }

  function createInsert(object){
    var ddl = "insert into " + thisModel.name + "( ";
    var keys = Object.keys(thisModel.columns);
    var objectKeys = Object.keys(object);
    var orderedKeys = []
    objectKeys.forEach(function(key, i){
      if(keys.indexOf(key) > -1){
        orderedKeys.push(key)
        ddl +=  key + (i === objectKeys.length-1? " " : ", ");
      }else{
        throw Error('unknown key')
      }
    })
    ddl += " ) values ( ";
    objectKeys.forEach(function(key, i){
      if(keys.indexOf(key) > -1){
        ddl +=  "?" + (i === objectKeys.length-1? " " : ", ");
      }
    })
    ddl += " )";
    var params = createInsertParams(orderedKeys, object)
    return {
      statement: ddl,
      params: params
    }
  }

  function createInsertParams(orderedKeys, object){
    var params = orderedKeys.map(function(key){
      return object[key];
    });
    return params;
  }

  function createUpdateParams(orderedKeys, object) {
    var primary = getPrimary();
    var params = [];
    //console.log('primary ', primary )
    //console.log('object ', object )
    //console.log('orderedKeys ', orderedKeys )
    orderedKeys.forEach(function(key){
      if(key !== primary){
        params.push(object[key])
      }
    });
    params.push(object[primary])
    return params;
  }

  function createUpdate(object){
    var ddl = "update " + thisModel.name + " set ";
    var objectKeys = Object.keys(object);
    var keys = Object.keys(thisModel.columns);
    var orderedKeys = [];
    objectKeys.filter(function(key) {
      return !thisModel.columns[key].primarykey
    }).forEach(function(key, i){
      if(keys.indexOf(key) > -1){
        orderedKeys.push(key)
        ddl +=  key + (i === objectKeys.length-2? "=? " : "=?, ");
      }else{
        throw Error('unknown key')
      }
    })
    ddl += " where " + getPrimary() + "=?"
    var params = createUpdateParams(orderedKeys, object)
    return {
      statement: ddl,
      params: params
    }
  }

  function createDelete(){
    var ddl = "delete from " + thisModel.name;
    var primary = getPrimary();
    var keys = Object.keys(thisModel.columns);
    ddl += " where " + primary + "=?"
    return {
      statement: ddl,
      params: []
    }
  }


  function getPrimary(){
    var primaryKeyName = ''
    var keys = Object.keys(thisModel.columns);
    keys.forEach(function(key) {
      if(thisModel.columns[key].primarykey){
        primaryKeyName = key
      }
    })
    return primaryKeyName;
  }

  return {
    select: createSelect,
    selectOne: createSelectOne,
    insert: createInsert,
    update: createUpdate,
    delete: createDelete
  }

}

module.exports = ddl;
