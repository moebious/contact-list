


function batchHelper(cqlHelper, idService){

  function createBatches(object) {
    var queries = [];
    queries.push({
      query: "delete from tag where contactid = ?",
      params: [object.contactid]
    })
    var words = splitToWords(object)
    words.forEach(function(word){
      var insertedTag = {
        tagid: idService.Id(),
        contactid: object.contactid,
        word: word
      }
      queries.push({
        query: cqlHelper.insert(insertedTag).statement,
        params: cqlHelper.insert(insertedTag).params
      })
    })
    return queries;
  }

  function splitToWords(object){
    var words = Object.keys(object)
    .map(function(key) {
      return object[key];
    })
    .reduce(function(words, keyValue, i) {
      return words.concat(keyValue.split(' '));
    }, [])

    return words;
  }

  return {
    batches: createBatches
  }

}

module.exports = batchHelper;
