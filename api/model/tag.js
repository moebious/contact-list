module.exports = {
  name: "tag",
  columns: {
    tagid: {
      type: "string",
      primarykey: true
    },
    contactid: {
      type: "string"
    },
    word: {
      type: "string"
    }
  }
}
