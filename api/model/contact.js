

module.exports = {
  name: "contacts",
  columns: {
    id: {
      type: "string",
      primarykey: true
    },
    fname: {
      type: "string"
    },
    lname: {
      type: "string"
    },
    addr1:{
      type: "string"
    },
    addr2:{
      type: "string"
    },
    city:{
      type: "string"
    },
    state: {
      type: "string"
    },
    zip: {
      type: "string"
    },
    email: {
      type: "string"
    },
    comments: {
      type: "string"
    },
    phone: {
      type: "string"
    }
  }
}
