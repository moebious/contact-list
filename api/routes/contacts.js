var express = require('express');
var router = express.Router();
var cassandra = require('cassandra-driver');
var client = new cassandra.Client({ contactPoints: ['127.0.0.1'], keyspace: "contactdemo"});
var db = require('../service/dbClient');
var model = require('../model/contact');
var tag = require('../model/tag');

var helper = require('../service/queryHelper');
var batchHelper = require('../service/batchHelper');


var idService = require('../service/idService');
var tagCqlHelper = helper(tag);
var batches = batchHelper(tagCqlHelper, idService);

var database = db(client);
var sql = helper(model);

/* GET home page. */
router.route('/contacts')
.get(function(req, res, next) {
  //get a list of contacts
  database.exec(sql.select(), null, null)
    .then(function(result) {
      var contacts = result.rows;
      res.send(contacts)
    })
    .catch(function(err) {
      res.status(500).send(err)
    });
})
.post(function(req, res, next) {
  //create a contact
  try{
    var contact = req.body;
    contact.id = idService.Id();
    var insert = sql.insert(contact);
    database.exec(insert.statement, insert.params, null)
      .then(function(result) {
        result.contact = contact;
        res.send(result)
      })
      .catch(function(err) {
        res.status(500).send(err)
      });

  }catch(e){

    res.status(500).send({
      message: e.message
    })
  }

});

router.route('/contacts/:id')
.get(function(req, res, next) {
  database.exec(sql.selectOne(), [req.params.id], null)
    .then(function(result){
      res.send(result.rows[0])
    })
    .catch(function(err) {
      res.status(500).send(err)
    });
})
.put(function(req, res, next) {
  //update a contact
  var contact = req.body;
  contact.id = req.params.id
  var update = sql.update(contact)
  console.log(update.statement)
  console.log(update.params)
  database.exec(update.statement, update.params, null)
    .then(function(result) {
      result.contact = contact
      res.send(result)
    })
    .catch(function(err) {
      res.status(500).send(err)
    });
})
.delete(function(req, res, next) {
  //delete a contact
  console.log(sql.delete().statement)
  database.exec(sql.delete().statement, [req.params.id], null)
    .then(function(result) {
      res.send(result)
    })
    .catch(function(err) {
      res.status(500).send(err);
    })
})

router.route('/contacts/search')
.get(function(req, res, next) {
  var searchTerm = req.query.q;
  res.send({message: "searching contacts to match the criteria given " + searchTerm })
})

module.exports = router;
