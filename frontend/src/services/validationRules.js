let contactRules =  [{
     name: 'fname',
     display: 'first name',
     rules: 'required'
 }, {
     name: 'lname',
     display: 'last name',
     rules: 'required'
 }, {
     name: 'addr1',
     display: 'address line 1',
     rules: 'required'
 }, {
     name: 'city',
     display: 'city',
     rules: 'required'
 }, {
     name: 'state',
     display: 'state',
     rules: 'required'
 }, {
     name: 'zip',
     display: 'zip code',
     rules: 'required'
 }, {
     name: 'email',
     display: 'email address',
     rules: 'required'
 }];

 export default contactRules;
