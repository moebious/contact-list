import React from 'react';
import ReactDOM from 'react-dom';
import App from './modules/App';
import NewContact from './modules/NewContact';
import ContactList from './modules/ContactList';
import Contact from './modules/Contact';
import Index from './modules/Index';
import NotFound from './modules/NotFound';
import { browserHistory
  , Router
  , Route
  , IndexRoute } from 'react-router'


ReactDOM.render((
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={ContactList} />
      <Route path="contact/new" component={NewContact} />
      <Route path="contact/:id" component={Contact} />
      <Route path="*" component={NotFound} />
    </Route>
  </Router>
), document.getElementById('app'))
