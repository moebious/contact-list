import React from 'react';
import { Link } from 'react-router'
import ContactStore from './ContactStore';
import { Input, ButtonInput, Alert} from 'react-bootstrap';
import FormValidator from 'validate-js'
import contactRules from '../services/validationRules';
import states from '../services/states'



class NewContact extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      fname: "",
      lname: "",
      addr1: "",
      addr2: "",
      city: "",
      state: "",
      zip: "",
      email: "",
      phone: "",
      comments: "",
      messages: []
    }
    this.validator = null;
  }
  componentDidMount = () => {
    var self = this;
    this.validator = new FormValidator('contactForm', contactRules, function(errors, event) {
       var messages = [];
       if (errors.length > 0) {
         errors.forEach(e => {
           messages.push(e.message)
         })
         self.setState({
           messages: messages
         })

       }else{
         event.preventDefault()

         var contact = {
           fname: self.state.fname,
           lname: self.state.lname,
           addr1: self.state.addr1,
           addr2: self.state.addr2,
           city: self.state.city,
           state: self.state.state,
           zip: self.state.zip,
           email: self.state.email,
           phone: self.state.phone,
           comments: self.state.comments
         }
         ContactStore.addContact(contact, (err, contact) => {
           if(err){
             messages.push(err.message || "an error has occured")
             self.setState({
               messages: messages
             })
           }
           self.context.router.push(`/contact/${contact.id}`)
         })
       }
   })
  };
  createContact = (e) => {
    //e.preventDefault();


  };

  handleChange = (e) => {
    let value = e.target.value;
    let name = e.target.name;
    let state = {}
    state[name] = value
    this.setState(state)
  };
  handleAlertDismiss = () => {
    this.setState({
      messages: []
    })

  };
  getAlert = () => {
    if(this.state.messages.length > 0){
      return(
        <Alert bsStyle="danger" onDismiss={this.handleAlertDismiss}>
          {this.state.messages.map((message, i) => {
            return <div key={i}>{message}</div>
          })}
        </Alert>
      )
    }
  };

  getStateOptions = (value) => {
    let options = states.map((item, index) => {
      return(
        <option
          key={index}
          value={item.abbr} >
          {item.name}
        </option>
      )
    })

    return options;
  };

  render() {
    console.log('messages ' ,this.state.messages)
   return (
     <div className="container">
      {this.getAlert()}

       <form name="contactForm"  onSubmit={this.createContact}>
           <Input type="text" label="First Name" bsSize="small" name="fname" id="fname" onChange={this.handleChange} placeholder="First Name" />
           <Input type="text" label="Last Name" bsSize="small" name="lname" id="lname" onChange={this.handleChange} placeholder="Last Name" />
           <Input type="text" label="Address Line 1" bsSize="small" name="addr1" id="addr1" onChange={this.handleChange} placeholder="Address Line 1" />
           <Input type="text" label="Address Line 2" bsSize="small" name="addr2" id="addr2" onChange={this.handleChange} placeholder="Address Line 2" />
           <Input type="text" label="City" bsSize="small" name="city" id="city" onChange={this.handleChange} placeholder="City" />
           <Input type="select" label="State" bsSize="small" name="state" id="state" onChange={this.handleChange} placeholder="State" >
             {this.getStateOptions()}
           </Input>
           <Input type="text" label="Zip" bsSize="small" name="zip" id="zip" onChange={this.handleChange} placeholder="Zip" />
           <Input type="text" label="Email" bsSize="small" name="email" id="email" onChange={this.handleChange} placeholder="Email" />
           <Input type="text" label="Phone" bsSize="small" name="phone" id="phone" onChange={this.handleChange} placeholder="Phone" />
           <Input type="textarea" label="Comments" bsSize="small" name="comment" id="comments" onChange={this.handleChange} placeholder="Comments" />
           <ButtonInput type="submit" className="btn btn-default btn-block" bsSize="small" value="Save Contact" />
           <ButtonInput bsSize="small" className="btn btn-warning btn-block" value="Cancel" />
       </form>
     </div>
   )
 }
}
NewContact.contextTypes = {
     router: React.PropTypes.object.isRequired
};
export default NewContact;
