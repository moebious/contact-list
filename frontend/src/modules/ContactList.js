import React from 'react';
import ContactStore from './ContactStore';
import { Link } from 'react-router'
import TableRow from './TableRow'

class ContactList extends React.Component {
  constructor(props){
    super(props)
    this.state =  {
      contacts: ContactStore.getContacts(),
      loading: true
    }
  }
  componentWillMount = () => {
    ContactStore.init()
  };
  componentDidMount = () => {
    ContactStore.addChangeListener(this.updateContacts)
  };
  componentWillUnmount = () => {
    ContactStore.removeChangeListener(this.updateContacts)
  };
  updateContacts = () => {
    //if(!this.isMounted()){
    //  return
    //}
    this.setState({
      contacts: ContactStore.getContacts(),
      loading: false
    })
  };
  render(){
    console.log('contacts ', this.state.contacts)
   return (
     <div className="container">
       <div>
         <Link className="btn btn-primary btn-sm" to="/contact/new">New Contact</Link>
         {this.state.contacts.length > 0 &&
           <table className="table table-striped table-hover">
               <tbody>
                  {this.state.contacts.map((contact, i) => {
                    return (
                      <tr key={i}>
                        <td>{contact.lname || ''}</td>
                        <td>{contact.fname || ''}</td>
                        <td>{contact.email || ''}</td>
                        <td>{contact.phone || ''}</td>
                        <td><Link className="btn btn-primary btn-xs" to={`/contact/${contact.id}`}>Edit</Link></td>
                       </tr>
                     )
                  })}
               </tbody>
            </table>
          }
       </div>

     </div>
   )
  }
}

export default ContactList;
