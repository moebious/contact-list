import React from 'react';
import ContactStore from './ContactStore';
import { Input, ButtonInput, Alert, Label} from 'react-bootstrap';
import FormValidator from 'validate-js'
import contactRules from '../services/validationRules';
import states from '../services/states'

class Contact extends React.Component {
  constructor(props){
    super(props)
    let contact = this.getStateFromStore();
    this.state = {
        id: contact.id,
        fname: contact.fname,
        lname: contact.lname,
        addr1: contact.addr1,
        addr2: contact.addr2,
        city: contact.city,
        state: contact.state,
        zip: contact.zip,
        email: contact.email,
        phone: contact.phone,
        comments: contact.comments,
        messages: []
      }
    this.validator = null;
  }
  componentDidMount = () => {
    ContactStore.addChangeListener(this.updateContact)
    var self = this;
    this.validator = new FormValidator('contactForm', contactRules, function(errors, event) {
       var messages = [];
       if (errors.length > 0) {
         errors.forEach(e => {
           messages.push(e.message)
         })
         self.setState({
           messages: messages
         })

       }else{
         event.preventDefault()

         var contact = {
           id: self.state.id,
           fname: self.state.fname,
           lname: self.state.lname,
           addr1: self.state.addr1,
           addr2: self.state.addr2,
           city: self.state.city,
           state: self.state.state,
           zip: self.state.zip,
           email: self.state.email,
           phone: self.state.phone,
           comments: self.state.comments
         }
         ContactStore.updateContact(contact, (err, contact) => {
           if(err){
             messages.push(err.message || "an error has occured")
             self.setState({
               messages: messages
             })
           }
           self.context.router.push('/')
         })
       }
   })
  };

  componentWillUnmount = () => {
    ContactStore.removeChangeListener(this.updateContact)
  };

  getStateFromStore = (props) => {
    const { id } = props ? props.params : this.props.params
    return ContactStore.getContact(id)

  };

  updateContact = () => {
    let contact = this.getStateFromStore()
    if(contact){
      this.setState({
          id: contact.id,
          fname: contact.fname,
          lname: contact.lname,
          addr1: contact.addr1,
          addr2: contact.addr2,
          city: contact.city,
          state: contact.state,
          zip: contact.zip,
          email: contact.email,
          phone: contact.phone,
          comments: contact.comments
        })
    }

  };

  destroy = () => {
    const { id } = this.props.params
    let self = this;
    ContactStore.removeContact(id, () =>{
      self.context.router.push('/')
    })


  };
  handleChange = (e) => {
    let value = e.target.value;
    let name = e.target.name;
    let state = {}
    state[name] = value
    this.setState(state)
  };
  handleAlertDismiss = () => {
    this.setState({
      messages: []
    })

  };
  getAlert = () => {
    if(this.state.messages.length > 0){
      return(
        <Alert bsStyle="danger" onDismiss={this.handleAlertDismiss}>
          {this.state.messages.map((message, i) => {
            return <div key={i}>{message}</div>
          })}
        </Alert>
      )
    }
  };

  getStateOptions = (value) => {
    let options = states.map((item, index) => {
      return(
        <option
          key={index}
          value={item.abbr} >
          {item.name}
        </option>
      )
    })

    return options;
  }


  render() {
    console.log(this.state)
    let name = this.state.fname + ' ' + this.state.lname
    return (
      <div className="container">
        {this.getAlert()}
        <h3>{name}</h3>
        <h4>ID:<Label>{this.state.id}</Label></h4>
        <form name="contactForm">
          <Input type="text" value={this.state.fname} label="First Name" bsSize="small" name="fname" id="fname" onChange={this.handleChange} placeholder="First Name" />
          <Input type="text" value={this.state.lname}  label="Last Name" bsSize="small" name="lname" id="lname" onChange={this.handleChange} placeholder="Last Name" />
          <Input type="text" value={this.state.addr1}  label="Address Line 1" bsSize="small" name="addr1" id="addr1" onChange={this.handleChange} placeholder="Address Line 1" />
          <Input type="text" value={this.state.addr2}  label="Address Line 2" bsSize="small" name="addr2" id="addr2" onChange={this.handleChange} placeholder="Address Line 2" />
          <Input type="text" value={this.state.city}  label="City" bsSize="small" name="city" id="city" onChange={this.handleChange} placeholder="City" />
          <Input type="select" value={this.state.state}  label="State" bsSize="small" name="state" id="state" onChange={this.handleChange} placeholder="State" >
            {this.getStateOptions()}
          </Input>
          <Input type="text" value={this.state.zip}  label="Zip" bsSize="small" name="zip" id="zip" onChange={this.handleChange} placeholder="Zip" />
          <Input type="text" value={this.state.email}  label="Email" bsSize="small" name="email" id="email" onChange={this.handleChange} placeholder="Email" />
          <Input type="text" value={this.state.phone}  label="Phone" bsSize="small" name="phone" id="phone" onChange={this.handleChange} placeholder="Phone" />
          <Input type="textarea" value={this.state.comments}  label="Comments" bsSize="small" name="comments" id="comment" onChange={this.handleChange} placeholder="Comments" />
          <ButtonInput type="submit" className="btn btn-default btn-block" bsSize="small" value="Save Contact" />
          <ButtonInput className="btn btn-default btn-block" bsSize="small" value="Delete"  onClick={this.destroy} />
        </form>
      </div>
    )
  }
}

Contact.contextTypes = {
     router: React.PropTypes.object.isRequired
};

export default Contact;
