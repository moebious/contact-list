import React from 'react';
import ContactStore from './ContactStore';
import Index from './Index'
import { Link } from 'react-router'

class App extends React.Component {
  constructor(props){
    super(props)
  }
  render(){

   return (
     <div className="App">
       <Index />
       <div className="Content">
         {this.props.children}
       </div>
     </div>
   )
  }
}

export default App;
