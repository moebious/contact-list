import React from 'react';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';

class Index extends React.Component {
  handleSelect = (event, eventKey) => {
    this.context.router.push('/contact/new')
  };
  handleListContacts = (event, eventKey) => {
    this.context.router.push('/')
  };
  render(){
    return (
      <Navbar default>
        <Navbar.Header>
          <Navbar.Brand>
            <a href="/">Cassandra Contact List</a>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <NavItem eventKey={1} onSelect={this.handleSelect} href="#">New Contact</NavItem>
            <NavItem eventKey={1} onSelect={this.handleListContacts} href="#">List Contacts</NavItem>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    )
  }
}
Index.contextTypes = {
     router: React.PropTypes.object.isRequired
};

export default Index;
